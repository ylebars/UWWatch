UWWatch desk
============

[**UWWatch desk**](https://framagit.org/ylebars/UWWatch "UWWatch desk")
(unstructured
[WaveWatch III<sup>Ⓡ</sup>](http://polar.ncep.noaa.gov/waves/wavewatch/ "WaveWatch III")
desktop utilities) is a collection of tools within a unified graphical
interface. It aims to ease generating (unstructured) meshes and
configuration files for WaveWatch III<sup>Ⓡ</sup>, as well as analyse its
outputs.

Getting started
---------------

**UWWatch desk** runs on any system supported by
[Python 3](https://www.python.org/ "Python scripting language") and
[Qt 5](https://www.qt.io/ "Qt"), including, but not limited to,
[GNU](https://www.gnu.org/ "The GNU project")/[Linux](https://www.kernel.org/ "The Linux Kernel"),
[*BSD](http://www.bsd.org/ "BSD"),
[Oracle Solaris<sup>Ⓡ</sup>](https://www.oracle.com/solaris/ "Oracle Solaris"),
[MacOS<sup>Ⓡ</sup>](https://www.apple.com/macos/ "MacOS") and
[Microsoft Windows<sup>Ⓡ</sup>](http://windows.microsoft.com/ "Microsoft Windows").
To run **UWWatch desk**, you need a Python 3 interpreter. It uses the
following modules:

* [NumPy](http://www.numpy.org/ "NumPy");
* [SciPy](https://www.scipy.org/ "SciPy");
* [Cartopy](https://scitools.org.uk/cartopy/docs/latest/ "Cartopy");
* [PyQt5](https://riverbankcomputing.com/software/pyqt/intro "PyQt5");
* [Python-magic](https://pypi.org/project/python-magic/ "Python-magic");
* [Hdf5storage](https://pypi.org/project/hdf5storage/ "Hdf5storage").

To launch **UWWatch desk**, simply run `uwwatch.py` in `main` directory.
On [POSIX<sup>Ⓡ</sup>](http://www3.opengroup.org/content/posix%C2%AE "POSIX")
compliant operating systems such as GNU/Linux, *BSD, Oracle Solaris<sup>Ⓡ</sup>,
or MacOS<sup>Ⓡ</sup>, I recommand you make a dynamic link to it in a directory
that is set in your `$PATH` environment variable, for instance in
`~/bin`. To create this directory:

	$ mkdir ~/bin

To add this directory to your `$PATH` environment variable, for instance if you
are using [Bash](https://www.gnu.org/software/bash/ "Bash"), edit the file
`~/.bashrc` and add the following:

	export PATH=$PATH:~/bin

To make a dynamic link:

	$ link -s /path/to/UWWatch/main/uwwatch.py ~/bin/uwwatch

You can then run **UWWatch desk** in command line:

	$ uwwatch

On [Freedesktop](https://www.freedesktop.org/wiki/ "Freedesktop.org")
compliant desktop environments such as [Gnome](https://www.gnome.org/ "Gnome"),
[KDE](https://www.kde.org/ "KDE"), or [XFCE](https://xfce.org/ "XFCE"), you can
create a menu entry, for instance using
[MenuLibre](https://bluesabre.org/projects/menulibre/ "MenuLibre").

Licence
-------

This software is distributed under CeCILL-B V1 licence, it can be
copied and modified freely as long as initial authors are cited.
Complete text of the licence is in `LICENSE` file and can be found
[on-line](http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html "CeCILL-B").

Application icon:
[The Great Wave off Kanagawa](https://en.wikipedia.org/wiki/The_Great_Wave_off_Kanagawa "The Great Wave off Kanagawa")
by
[Hokusai, Katsushika](https://en.wikipedia.org/wiki/Hokusai "Hokusai, Katsushika"),
public domain image via
[Wikimedia Commons](https://commons.wikimedia.org/wiki/File:The_Great_Wave_off_Kanagawa.jpg?uselang%20=%20en "The image on Wikimedia Commons.").

Open icon by
[Stawarz, Benjamin](https://www.iconfinder.com/butterflytronics "Icons by Stawarz, Benjamin") from
[Iconfinder](https://www.iconfinder.com/ "Iconfinder.com") under
[Creative Commons (Attribution 3.0 Unported)](https://creativecommons.org/licenses/by/3.0/ "CC BY 3.0")
licence.

Close and exit icon by
[Khamitov, Artyom](https://www.iconfinder.com/Kh.Artyom "Icons by Khamitov, Artyom")
from [Iconfinder](https://www.iconfinder.com/ "Iconfinder.com") under
[Creative Commons (Attribution 3.0 Unported)](https://creativecommons.org/licenses/by/3.0/ "CC BY 3.0")
licence.

WaveWatch III is a registered trademark owned by the
[National Weather Service](https://www.weather.gov/ "NWS")
(NWS),
[National Oceanic and Atmospheric Administration](http://www.noaa.gov/ "NOAA").

POSIX is a registered trademark owned by
[the Open Group](http://opengroup.org/ "The Open Group").

Linux is a registered trademark owned by
[Torvalds, Linus](https://github.com/torvalds "Torvalds, Linus on Github.").

Oracle Solaris is a registered trademark owned by
[Oracle](https://www.oracle.com/ "Oracle").

MacOS is a registered trademark owned by
[Apple](https://www.apple.com/ "Apple").

Microsoft Windows is a registered trademark owned by
[Microsoft](https://www.microsoft.com/ "Microsoft").

Copyright © 2018-2019 [Le Bars, Yoann](http://le-bars.net/yoann/ "A View from here").
