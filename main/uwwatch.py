#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Graphic interface unifying tools collection for WaveWatch3 unstructured
version.

:module: uwwatch
:author: Le Bars, Yoann
:version: 0.5
:date: 2018/06/15

Copyright © 2018 Le Bars, Yoann

This program is distributed under CeCILL-B license, it can be copied and
modified freely as long as initial author is cited. Complete text
of CeCILL license can be found on-line:

<http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>
"""

# Version major.
__major__ = '0'
# Version minor
__minor__ = '7'
# Program version.
__version__ = __major__ + '.' + __minor__

from os import path, listdir
import sys

# Directory in which is current script.
currDir = path.dirname(path.realpath(__file__))
# Directory containing plugins.
piDir = path.normpath(currDir + '/../plugins')
# Resources directory.
resDir = path.normpath(currDir + '/../resources')
# Add plugins directory in list of directory in for modules inclusion.
sys.path.insert(1, piDir)
# Name of the directory containing utilities for plugins implementation.
pluginsUtils = 'PluginsUtils'
sys.path.insert(1, piDir + '/' + pluginsUtils)
import PluginsUtils

from PyQt5 import QtCore, uic, QtWidgets
import magic
from importlib import import_module
import imp

class MainWindow (QtWidgets.QMainWindow):
    """
    Application main class.
    """

    def _about (self):
        """
        Displaying information concerning the application.
        """

        # About file name.
        aboutName = 'about_' + self._locale + '.htm'
        # Resource files list.
        resList = [elem for elem in listdir(resDir)
                   if path.isfile(resDir + '/' + elem)]
        if not aboutName in resList:
            aboutName = 'about.htm'

        # Message box title.
        title = self.translate('MainWindow', 'About UWWatch desk')
        with open(resDir + '/' + aboutName, 'r') as aboutFile:
            # About message.
            message = aboutFile.read().format(__version__)
        # The about widget.
        QtWidgets.QMessageBox.about(self, title, message)
        self.statusBar.showMessage(self.readyMessage)

    def displayWarning (self, title, message):
        """
        Open a message box with a warning message.

        :param title: Title of the displaying message box.
        :param message: Message to be displayed.
        """

        # New message box.
        dialog = QtWidgets.QMessageBox(QtWidgets.QMessageBox.Warning, title,
                                       message, parent = self)
        dialog.exec_()

    def _unsupportedFormat (self):
        """
        Display a message indicating the file format is not supported.
        """

        # Title of the message box.
        title = self.translate('MainWindow', 'Unsupported format')
        # Message to be displayed.
        message = self.translate('MainWindow', '<p>File format not '
                                               'supported.</p>')
        self.displayWarning(title, message)

    def _openFile (self, fileName):
        """
        Open file using convenient plugin.
        
        :param fileName: Name of the file to be opened.
        """

        # File MIME type.
        mimeType = magic.detect_from_filename(fileName).mime_type
        # File extension.
        extension = '*' + path.splitext(fileName)[1]

        # Detected format.
        detectedFormat = 'unsupported'

        # Test file with every supported formats.
        for fileFormat in self._supportedFormats:
            if (mimeType == fileFormat[0]) and (extension in fileFormat[1]):
                detectedFormat = fileFormat[2]
                break

        if self._isOpen:
            # New window instance.
            newWindow = MainWindow([fileName], self._appName, self._locale,
                                   self._windowsList, parent = self)
            self._windowsList.append(newWindow)

        else:
            self.setWindowTitle(path.basename(fileName) + ' – '
                                + self._appName)

            try:
                self._openSelector.get(detectedFormat)(fileName)

            except PluginsUtils.FileError as fileError:
                # Title o the message box.
                title = self.translate('MainWindow', 'Invalid file')
                # Message to be displayed.
                message = self.translate('MainWindow',
                                         '<p>Invalid format in file named '
                                         '“{}”.</p>')
                self.displayWarning(title, message.format(fileError.fileName))

            except PluginsUtils.UnsupportedVersion as uVersion:
                # Title of the message box.
                title = self.translate('MainWindow', 'Unsupported version')
                # Message to be displayed.
                message = self.translate('MainWindow',
                                         '<p>File named “{}” is in format {} '
                                         'version {}, which is not '
                                         'supported.</p>')
                self.displayWarning(title,
                                     message.format(uVersion.fileName,
                                                    uVersion.fileFormat,
                                                    uVersion.version))

            except PluginsUtils.InexistingFile as iFile:
                # Title of the message box.
                title = self.translate('MainWindow', 'Invalid file')
                # Message to be displayed.
                message = self.translate('MainWindow', '<p>File named “{}” '
                                         'is invalid.</p>')
                self.displayWarning(title, message.format(iFile.fileName))

            except PluginsUtils.InexistingField as iField:
                # Message box title.
                title = self.translate('MainWindow', 'Invalid field')
                # Message to be displayed.
                message = self.translate('MainWindow', '<p>Field named “{}” '
                                         'does not exists in file “{}”.</p>')
                self.displayWarning(title,
                                    message.format(iField.fieldName,
                                                   iField.fileName))

            except PluginsUtils.PluginError as error:
                # Title of the message box.
                title = self.translate('MainWindow', 'Error')
                self.displayWarning(title, error.message)

            else:
                self._isOpen = True

            finally:
                self.statusBar.showMessage(self.readyMessage)

    def _openMenu (self):
        """
        Launch file opening menu.
        """

        # Status message.
        status = self.translate('MainWindow', 'Opening file.')
        self.statusBar.showMessage(status)

        # Options for file dialog.
        options = QtWidgets.QFileDialog.Options()
        # Title of the open file window.
        title =  self.translate('MainWindow', 'Open file')

        # All files.
        allFiles = self.translate('MainWindow', 'All files (*)')
        # Possible files choices.
        choices = ''
        for fileFormat in self._supportedFormats:
            choices = choices + fileFormat[2] + ' ('
            for i in range(len(fileFormat[1]) - 1):
                choices = choices + fileFormat[1][i] + ' '
            choices = choices + fileFormat[1][-1] + ');;'
        choices = choices + allFiles

        # Name of the file to be opened.
        fileName, __ = QtWidgets.QFileDialog.getOpenFileName(self, title, '',
                                                             choices,
                                                             options = options)
        if fileName != '':
            self._openFile(fileName)

    def _closeWindow (self):
        """
        Closing a window, if there is no more windows, close application.
        """

        self._windowsList.remove(self)
        if len(self._windowsList) == 0:
            QtWidgets.qApp.quit()
        else:
            self.destroy()

    def _connectActions (self):
        """
        Associating actions to the different elements in the interface.
        """

        self._ui.actionQuit.triggered.connect(QtWidgets.qApp.quit)
        self._ui.actionAbout.triggered.connect(self._about)
        self._ui.actionAboutQt.triggered.connect(QtWidgets.qApp.aboutQt)
        self._ui.actionOpen.triggered.connect(self._openMenu)
        self._ui.actionCloseWindow.triggered.connect(self._closeWindow)

    def _loadPlugins (self):
        """
        Loads application plugins.
        """

        # Plugins list.
        pluginsList = sorted([elem for elem in listdir(piDir)
                              if path.isdir(piDir + '/' + elem)])
        pluginsList.remove(pluginsUtils)
        # List of format supported by the application.
        self._supportedFormats = []

        # Opening procedures selector.
        self._openSelector = {
            'unsupported': (lambda dummy: self._unsupportedFormat())
        }

        def formatHandler (module):
            """
            Loading a format handler plugin.

            :param module: Pointer to the module implementing the handler.
            """

            # File format handler.
            handler = module.Main(self)
            # Format descriptor.
            fileDesc = module.formatDescriptor
            self._supportedFormats.append(fileDesc)
            self._openSelector[fileDesc[2]] = handler.open

        def utilityPlugin (module):
            """
            Loading an utility plugin.

            :param module: Pointer to the module implementing the utility.
            """

            pass

        # Dictionary to select what to be done.
        actionSelector = {
            PluginsUtils.ProvideEnum.Utils: utilityPlugin,
            PluginsUtils.ProvideEnum.FileFormat: formatHandler
        }

        # Loading all plugins.
        for plugin in pluginsList:
            sys.path.insert(1, imp.find_module(plugin)[1])
            # Module implementing the plugin.
            module = import_module(plugin)
            actionSelector.get(module.provide)(module)

    def __init__ (self, oFilesNames, appName, locale, windowsList,
                  parent = None, **kwargs):
        """
        Class constructor (initializer).

        :param oFilesNames: List of name for files to open.
        :param resDir: Path to resources directory.
        :param appName: Name of the application.
        :param locale: System localisation.
        :param windowsList: List of opened windows.
        :param parent: Pointer to potential parent object.
        :param kwargs: command line arguments.
        """

        super(MainWindow, self).__init__(parent, **kwargs)
        # Translator shortcut.
        self.translate = QtCore.QCoreApplication.translate
        # Name of the interface description file.
        uiFile = resDir + '/main.ui'
        # Interface instantiation.
        self._ui = uic.loadUi(uiFile, self)
        # Application status bar.
        self.statusBar = self._ui.statusbar

        # Message indicating application state.
        currentState = self.translate('MainWindow', 'Initialisation.')
        self.statusBar.showMessage(currentState)
        self._ui.show()

        # Organisation name.
        organisationName = 'CNRS'
        # Application name.
        self._appName = appName
        QtCore.QCoreApplication.setOrganizationName(organisationName)
        QtCore.QCoreApplication.setOrganizationDomain('cnrs.fr')
        QtCore.QCoreApplication.setApplicationName(self._appName)
        QtCore.QCoreApplication.setApplicationVersion(__version__)
        # Application configuration.
        self._configuration = QtCore.QSettings(organisationName, self._appName)

        # Message indicating the application is ready.
        self.readyMessage = self.translate('MainWindow', 'Ready.')
        # Application drawing area.
        self.drawingArea = self._ui.drawingArea
        # Figure layout.
        self.figureLayout = self._ui.figureLayout
        # Application tool bar.
        self.toolBar = self._ui.toolBar
        # Folder where current script is located.
        self._currDir = currDir
        # Is a file open?
        self._isOpen = False
        # List of opened windows.
        self._windowsList = windowsList
        # System localisation.
        self._locale = locale

        self._loadPlugins()
        self._connectActions()

        for file in oFilesNames:
            if (not path.exists(file)) or (not path.isfile(file)):
                # Title of the message box.
                title = self.translate('MainWindow', 'Invalid file')
                # Message to be displayed.
                message = self.translate('MainWindow',
                                         'Invalid file: {}'.format(file))
                self.displayWarning(title, message)
            else:
                self._openFile(file)

        self.statusBar.showMessage(self.readyMessage)

    def __enter__ (self):
        """
        Return current instance.

        :return: A pointer to current instance.
        """

        return self

    def __exit__ (self, exc_type, exc_val, exc_tb):
        """
        Destructor (resources deallocation).

        :param exc_type: Type of launched exception, if any.
        :param exc_val: Exception value.
        :param exc_tb: Exception executing trace.
        """
        
        pass

def _main ():
    """
    Program main function.

    :return: 0 if everything went well, an error code otherwise.
    """

    import argparse

    # Application instance.
    app = QtWidgets.QApplication(sys.argv)
    # Application name.
    appName = 'UWWatch'
    app.setApplicationName(appName)
    app.setApplicationVersion(__version__)

    # Application description.
    desc = 'Collection of tools for WaveWatch III.'
    # Version help message.
    vHelp = 'Display program version and exit.'

    # Command line parser.
    parser = argparse.ArgumentParser(description = desc)
    parser.add_argument('-v', '--version', action = 'version',
                        version = '%(prog)s ' + __version__,
                        help = vHelp)
    parser.add_argument('files', metavar = 'fileName', type = str, nargs='*',
                        help = 'Names of files to be opened.')
    # Arguments in command line.
    args = parser.parse_args()

    # Get system localisation setting.
    locale = QtCore.QLocale.system().name()
    # Application translator.
    appTranslator = QtCore.QTranslator()
    if appTranslator.load(resDir + '/UWWatch_{}'.format(locale)):
        app.installTranslator(appTranslator)
    # Qt widgets translator.
    qtTranslator = QtCore.QTranslator()
    qtTranslator.load('qt_' + locale,
                      QtCore.QLibraryInfo.location(
                          QtCore.QLibraryInfo.TranslationsPath
                      ))
    app.installTranslator(qtTranslator)

    # List of windows.
    windowsList = []
    with MainWindow(args.files, appName, locale, windowsList) as mainWindow:
        windowsList.append(mainWindow)
        return app.exec_()

if __name__ == '__main__':
    sys.exit(_main())
