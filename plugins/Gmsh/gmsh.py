#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Module to display Gmsh meshes.

:module: Gmsh
:author: Le Bars, Yoann
:version: 1.1
:date: 2018/06/18

Copyright © 2018 Le Bars, Yoann

This program is distributed under CeCILL-B license, it can be copied and
modified freely as long as initial author is cited. Complete text
of CeCILL license can be found on-line:

<http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>
"""

from PyQt5 import QtCore
from PyQt5.QtWidgets import QCheckBox, QPushButton, QMessageBox
import matplotlib.tri as tri
import numpy as np
from matplotlib.pyplot import xlim, ylim
import statistics
import PluginsUtils

class Main (PluginsUtils.Main):
    """
    Class for mesh manipulation.
    """

    def _setPlot (self):
        """
        Create the figure to be displayed.
        """

        currentStatus = self._translate('Mesh', 'Drawing mesh.')
        self._statusBar.showMessage(currentStatus)

        # X label.
        xlabel = self._translate('Mesh', 'Longitude (degrees)')
        # Y label.
        ylabel = self._translate('Mesh', 'Latitude (degrees)')
        self._setAxis()
        if self._dispBath:
            # Figure title.
            figureTitle = self._translate('Mesh', 'Bathymetry (in m)')
            # Map bathymetry.
            tf = self._axis.tricontourf(self._tri_refi, self._z,
                                        levels = self._scaleLevels)
            self._colourBar = self._figure.colorbar(tf, ax = self._axis,
                                                    use_gridspec = True)
            self._isColourBar = True
        else:
            figureTitle = self._translate('Mesh', 'Mesh')
        #self._axis.triplot(self._mesh, 'bo-', lw = 1)
        self._axis.triplot(self._mesh, 'k-', lw = 0.5)
        if self._dispLabels:
            for i in range(len(self._nodes)):
                self._axis.text(self._nodes[i][0], self._nodes[i][1],
                                self._nodes[i][2] + 1, color = 'red',
                                fontsize = 13)
        self._axis.set_title(figureTitle)
        self._axis.set_xlabel(xlabel)
        self._axis.set_ylabel(ylabel)
        self._isAxis = True

        self.draw()

        self._statusBar.showMessage(self._readyMessage)

    def _clickBox (self, state):
        """
        Display bathymetry or not.

        :param state: Check box state.
        """

        if state == QtCore.Qt.Checked:
            self._dispBath = True
        else:
            self._dispBath = False

        if self._isAxis:
            self._axis.remove()
            if self._isColourBar:
                self._colourBar.remove()
                self._isColourBar = False
        self._setPlot()

    def _clickLabels (self, state):
        """
        Display node labels or not.

        :param state: Check box state.
        """

        if state == QtCore.Qt.Checked:
            self._dispLabels = True

            with open(self._meshFileName, 'r') as meshFile:
                # List of mesh abscissa.
                x = []
                # List of mesh ordinates.
                y = []

                # Current line to be processed.
                for line in meshFile:
                    if '$Nodes' in line:
                        line = meshFile.readline()
                        # Number of nodes in the mesh.
                        nNodes = int(line)
                        for i in range(nNodes):
                            line = meshFile.readline()
                            # Node descriptors.
                            ns, xs, ys, zs = line.split()
                            x.append(float(xs))
                            y.append(float(ys))

                # List of visible nodes.
                self._nodes = []
                # Zoom longitudes.
                lonMin, lonMax = xlim()
                # Zoom Latitudes.
                latMin, latMax = ylim()
                for i in range(nNodes):
                    if (lonMin <= x[i] <= lonMax) \
                        and (latMin <= y[i] <= latMax):
                            self._nodes.append([x[i], y[i], i])
                
        else:
            self._dispLabels = False
        self._setPlot()

    def _clickButton (self):
        """
        Compute mesh statistics and display them.
        """

        with open(self._meshFileName, 'r') as meshFile:
            # List of mesh abscissa.
            x = []
            # List of mesh ordinates.
            y = []
            # List of mesh elements.
            elements = []

            # Current line to be processed.
            for line in meshFile:
                if '$Nodes' in line:
                    line = meshFile.readline()
                    # Number of nodes in the mesh.
                    nNodes = int(line)
                    for i in range(nNodes):
                        line = meshFile.readline()
                        # Node descriptors.
                        ns, xs, ys, zs = line.split()
                        x.append(float(xs))
                        y.append(float(ys))

                elif '$Elements' in line:
                    line = meshFile.readline()
                    # Number of elements in the mesh.
                    nElem = int(line)
                    for i in range(nElem):
                        line = meshFile.readline()
                        # Description of the element.
                        elemDesc = line.split()
                        if elemDesc[1] == '2':
                            # First node in element.
                            node1 = int(elemDesc[-3]) - 1
                            # Second node in element.
                            node2 = int(elemDesc[-2]) - 1
                            # Third node in element.
                            node3 = int(elemDesc[-1]) - 1
                            elements.append([node1, node2, node3])

        # Zoom longitudes.
        lonMin, lonMax = xlim()
        # Zoom Latitudes.
        latMin, latMax = ylim()

        # Count how many nodes are visible.
        nodesCount = 0
        # List of nodes inside zoom.
        nodesList = []
        for i in range(nNodes):
            if (lonMin <= x[i] <= lonMax) and (latMin <= y[i] <= latMax):
                nodesCount += 1
                nodesList.append(i)
        # Count how many elements are visible.
        elementsCount = 0
        # List of elements size.
        sizes = []
        for i in range(len(elements)):
            node1 = elements[i][0]
            node2 = elements[i][1]
            node3 = elements[i][2]
            # Whether or not node1 is in zoom box.
            isNode1 = (lonMin <= x[node1] <= lonMax) \
                and (latMin <= y[node1] <= latMax)
            # Whether or not node2 is in zoom box.
            isNode2 = (lonMin <= x[node2] <= lonMax) \
                and (latMin <= y[node2] <= latMax)
            # Whether or not node3 is in zoom box.
            isNode3 = (lonMin <= x[node3] <= lonMax) \
                and (latMin <= y[node3] <= latMax)
            if isNode1 or isNode2 or isNode3:
                elementsCount += 1
                # Distance between the first two nodes
                dist1 = PluginsUtils.geoDist(x[node1], y[node1],
                                             x[node2], y[node2])
                # Distance between the second two nodes
                dist2 = PluginsUtils.geoDist(x[node2], y[node2],
                                             x[node3], y[node3])
                # Distance between the third two nodes
                dist3 = PluginsUtils.geoDist(x[node3], y[node3],
                                             x[node1], y[node1])
                sizes.append(dist1)
                sizes.append(dist2)
                sizes.append(dist3)

        minSize = min(sizes)
        maxSize = max(sizes)
        meanSize = np.mean(sizes)
        medSize = statistics.median(sizes)

        # Message box title.
        mBoxTitle = self._translate('Mesh', 'Mesh statistics')
        # Message box content.
        mBoxContent = self._translate('Mesh',
                                      '<p>Number of nodes: {}</p>'
                                      '<p>Number of triangular elements: {}</p>'
                                      '<p>Minimum element size: {} m</p>'
                                      '<p>Maximum element size: {} m</p>'
                                      '<p>Mean element size: {} m</p>'
                                      '<p>Median element size: {} m</p>'
                                      '<p>Zoom box min: {}° N, {}° E</p>'
                                      '<p>Zoom box max: {}° N, {}° E</p>')
        QMessageBox.about(self, mBoxTitle,
                          mBoxContent.format(nodesCount, elementsCount,
                                             minSize, maxSize, meanSize,
                                             medSize, latMin, lonMin, latMax,
                                             lonMax))

    def __init__ (self, parent, **kwargs):
        """
        Class initialiser.

        :param parent: Pointer to potential parent object.
        :param kwargs: command line arguments.
        """

        super(Main, self).__init__(parent, 'Gmsh')

        # Check box title.
        title = self._translate('Mesh', 'Display bathymetry')
        # Choose to display the bathymetry.
        self._bathyBox = QCheckBox(title, self._parent)
        # Whether or not display the bathymetry.
        self._dispBath = False
        # Whether or not there already is a plot.
        self._isAxis = False
        # Whether or not there is a colour bar.
        self._isColourBar = False
        self._bathyBox.stateChanged.connect(self._clickBox)

        title = self._translate('Mesh', 'Display node labels')
        # Choose to display node labels.
        self._nodeLabels = QCheckBox(title, self._parent)
        # Whether or not to display node labels.
        self._dispLabels = False
        self._nodeLabels.stateChanged.connect(self._clickLabels)

        title = self._translate('Mesh', 'Mesh statistics')
        # Choose to display mesh statistics.
        self._statButton = QPushButton(title)
        self._statButton.clicked.connect(self._clickButton)

    def open (self, meshFileName):
        """
        Open a Gmsh mesh file.

        :param fileName: Name of the file to be opened.
        """

        # Status message.
        currentStatus = self._translate('Mesh', 'Loading mesh file.')
        self._statusBar.showMessage(currentStatus)

        self._appToolBar.addWidget(self._bathyBox)
        self._appToolBar.addWidget(self._nodeLabels)
        self._appToolBar.addWidget(self._statButton)
        # Name of the file containing the mesh.
        self._meshFileName = meshFileName

        # Mesh file to be displayed.
        with open(meshFileName, 'r') as meshFile:
            # Current line to be processed.
            line = meshFile.readline()
            if not ('$MeshFormat' in line):
                raise PluginsUtils.FileError(meshFileName)

            line = meshFile.readline()
            # Format of current mesh.
            version = line.split()[0]
            if version[0] != '2':
                raise PluginsUtils.UnsupportedVersion(gmshId, version,
                                                      meshFileName)

            # List of mesh abscissa.
            x = []
            # List of mesh ordinates.
            y = []
            # Bathymetry.
            bath = []
            # List of mesh elements.
            self._elements = []
            # Whether or not nodes have been read.
            nodesReaded = False
            # Whether or not elements have been read.
            elementsReaded = False

            # Progress bar title.
            pBarTitle = self._translate('Mesh', 'Reading file.')
            # Progress bar indicating advancements in file processing.
            pBar = PluginsUtils.ProgressBar(pBarTitle, 0, 0)
            pBar.show()
            for line in meshFile:
                if '$Nodes' in line:
                    nodesReaded = True
                    line = meshFile.readline()
                    # Number of elements in the mesh.
                    nNodes = int(line)
                    for i in range(0, nNodes):
                        line = meshFile.readline()
                        # Node descriptors.
                        ns, xs, ys, zs = line.split()
                        x.append(float(xs))
                        y.append(float(ys))
                        bath.append(float(zs))
                    line = meshFile.readline()
                    if not ('$EndNodes' in line):
                        raise PluginsUtils.FileError(meshFileName)

                elif '$Elements' in line:
                    elementsReaded = True
                    line = meshFile.readline()
                    # Number of elements in the mesh.
                    nElem = int(line)
                    for i in range(0, nElem):
                        line = meshFile.readline()
                        # Description of the element.
                        elemDesc = line.split()
                        if elemDesc[1] == '2':
                            # First node in element.
                            node1 = int(elemDesc[-3]) - 1
                            node2 = int(elemDesc[-2]) - 1
                            node3 = int(elemDesc[-1]) - 1
                            self._elements.append([node1, node2, node3])
                    line = meshFile.readline()
                    if not ('$EndElements' in line):
                        raise PluginsUtils.FileError(meshFileName)
            pBar.cancel()

        if not nodesReaded:
            raise PluginsUtils.FileError(meshFileName)
        elif not elementsReaded:
            raise PluginsUtils.FileError(meshFileName)

        currentStatus = self._translate('Mesh', 'Creating figure.')
        self._statusBar.showMessage(currentStatus)

        self._setPlottingArea()

        # Mesh to be displayed.
        self._mesh = tri.Triangulation(x, y, self._elements)
        currentStatus = self._translate('Mesh', 'Interpolating bathymetry.')
        self._statusBar.showMessage(currentStatus)
        pBar.reset(currentStatus, 0, 0)
        pBar.show()
        # Refine data.
        refiner = tri.UniformTriRefiner(self._mesh)
        # Interpolated data.
        self._tri_refi, self._z = refiner.refine_field(bath, subdiv = 3)
        # Scale levels.
        self._scaleLevels = np.arange(min(bath), max(bath), 1)
        pBar.cancel()

        self._setPlot()

# What functionality this plugin provides.
provide = PluginsUtils.ProvideEnum.FileFormat
# Gmsh mesh file MIME type.
gmshMIME = 'text/plain'
# Gmsh mesh file extensions.
gmshExtensions = ['*.msh', '*.MSH', '*.gmsh', '*.GMSH']
# Gmsh mesh file identifier.
gmshId = QtCore.QCoreApplication.translate('MainWindow', 'Gmsh mesh')
# Gmsh mesh file describing tuple.
formatDescriptor = (gmshMIME, gmshExtensions, gmshId)
