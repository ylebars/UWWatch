#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Initialisation of NetCDF module.

:module: NetCDF
:author: Le Bars, Yoann
:version: 1.0
:date: 2018/06/23

Copyright © 2018 Le Bars, Yoann

This program is distributed under CeCILL-B license, it can be copied and
modified freely as long as initial author is cited. Complete text
of CeCILL license can be found on-line:

<http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>
"""

from netcdf import Main, formatDescriptor, provide
