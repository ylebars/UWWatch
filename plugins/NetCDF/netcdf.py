#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
NetCDF format handling plugin.

:module: NetCDF
:author: Le Bars, Yoann
:version: 1.1
:date: 2018/06/23

Copyright © 2018 Le Bars, Yoann

This program is distributed under CeCILL-B license, it can be copied and
modified freely as long as initial author is cited. Complete text
of CeCILL license can be found on-line:

<http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>
"""

from PyQt5 import QtCore, uic, Qt
from PyQt5.QtWidgets import QFileDialog, QPushButton
import matplotlib.tri as tri
from netCDF4 import Dataset
import numpy as np
from scipy import interpolate
from datetime import datetime, timedelta
from os import path
import PluginsUtils

def correctValues (values, x, y, vMin, vMax):
    """
    Function correcting extracted values.

    :param values: Extracted values.
    :param x: Nodes abscissa.
    :param y: Nodes ordinates.
    :param vMin: Minimum valid value.
    :param vMax: Maximum valid value.
    """

    values = np.asarray(values).flatten()
    # List of valid values.
    valid = np.isfinite(values)

    # Translating function.
    translate = QtCore.QCoreApplication.translate
    # Progress bar title.
    pBarTitle = translate('NetCDF', 'Correcting valid values.')
    # Progress bar that indicates progress in values interpolation.
    pBar = PluginsUtils.ProgressBar(pBarTitle)

    # Getting valid values.
    corrected = [min(max(value, vMin), vMax) for value in pBar(values[valid])]
    # Completed values.
    cValues = np.zeros(values.size)
    # Valid values counter.
    c = 0
    # Node list.
    points = np.array((x[valid].flatten(), y[valid].flatten())).T
    pBarTitle = translate('NetCDF', 'Interpolating invalid values.')
    pBar.reset(pBarTitle)
    for i in pBar(range(values.size)):
        if valid[i]:
            cValues[i] = corrected[c]
            c += 1
        else:
            cValues[i] = interpolate.griddata(points, corrected, (x[i], y[i]),
                                              method = 'cubic')

    return cValues

class Main (PluginsUtils.Main):
    """
    Class for NetCDF handling.
    """

    def _actualPlot (self):
        """
        Method which actually plot figure.
        """

        # The map being displayed.
        tf = self._axis.tricontourf(self._tri_refi, self._field,
                                    levels = self._scaleLevels)
        # Figure colour bar.
        self._colourBar = self._figure.colorbar(tf, ax = self._axis)

    def _setPlot (self):
        """
        Create the figure to be displayed.

        :param fieldName: Name of the field to be displayed.
        """

        # Status message.
        currentStatus = self._translate('NetCDF', 'Drawing plot.')
        self._statusBar.showMessage(currentStatus)

        self._setAxis()
        # The map being displayed.
        #tf = self._axis.tricontourf(self._tri_refi, self._field,
                                    #levels = self._scaleLevels)
        self._axis.set_title('{}'.format(self._times[self._iteration]))
        # X label.
        xlabel = self._translate('Mesh', 'Longitude (degrees)')
        # Y label.
        ylabel = self._translate('Mesh', 'Latitude (degrees)')
        self._axis.set_xlabel(xlabel)
        self._axis.set_ylabel(ylabel)
        self._actualPlot()
        self._colourBar.ax.set_title('{} ({})'.format(
            self._fieldName, self._values.units
        ))

        self.draw()

        self._statusBar.showMessage(self._readyMessage)

    def _computeScale (self):
        """
        Compute scale levels.
        """

        if self._scaleFileName == '':
            # Minimum value.
            minimum = min(self._cValues)
            # Maximum value.
            maximum = max(self._cValues)
            # Scale step.
            step = (maximum - minimum) / 30.
            # Scale levels.
            self._scaleLevels = np.arange(minimum - step, maximum + step,
                                          step)
        else:
            with open(self._scaleFileName) as scaleFile:
                # Scale levels.
                self._scaleLevels = [float(i)
                                     for i in scaleFile.read().split()]

    def _plot (self):
        """
        Plot asked field.
        """

        # Status message.
        currentStatus = self._translate('NetCDF', 'Computing interpolation.')
        self._statusBar.showMessage(currentStatus)

        # Progress bar title.
        pBarTitle = self._translate('NetCDF', 'Values interpolation.')
        # Progress bar that indicates progress in values interpolation.
        pBar = PluginsUtils.ProgressBar(pBarTitle)
        # Corrected values.
        self._cValues = [min(max(value, self._values.valid_min),
                                 self._values.valid_max)
                         for value in
                            pBar(PluginsUtils.interpolateGaps(
                                    self._values[self._iteration]))]
        #self._cValues = correctValues(self._values[self._iteration],
                                      #self._mesh.x, self._mesh.y,
                                      #self._values.valid_min,
                                      #self._values.valid_max)

        # Refine data.
        refiner = tri.UniformTriRefiner(self._mesh)
        # Interpolated data.
        self._tri_refi, self._field = refiner.refine_field(self._cValues,
                                                           subdiv = 3)

        #if self._scaleFileName == '':
            ## Minimum value.
            #minimum = min(cValues)
            ## Maximum value.
            #maximum = max(cValues)
            ## Scale step.
            #step = (maximum - minimum) / 30.
            ## Scale levels.
            #self._scaleLevels = np.arange(minimum - step, maximum + step,
                                          #step)
        #else:
            #with open(self._scaleFileName) as scaleFile:
                ## Scale levels.
                #self._scaleLevels = [float(i)
                                     #for i in scaleFile.read().split()]
        self._computeScale()

        self._setPlot()

    def _changeDate (self, index):
        """
        What to do when user changes date.

        :param index: Index of chosen value.
        """

        self._iteration = index
        self._colourBar.remove()
        self._axis.remove()
        self._plot()

    def _changeField (self, index):
        """
        What to do when user changes the field to be plotted.

        :param index: Index of chosen value.
        """

        self._fieldName = self._fieldChooser.itemText(index)
        self._colourBar.remove()
        self._axis.remove()
        self._plot()

    def _selectFile (self):
        """
        Procedure to select a file.
        """

        # Title for opening file window.
        title = self._translate('NetCDF', 'Open scale file')
        self._selectWindow.scaleChooser.document().setPlainText(
            QFileDialog.getOpenFileName(self, title)[0]
        )

    def _changeScale (self):
        """
        What to do when user changes the scale file.
        """

        self._selectFile()
        # New scale file name.
        newScaleFile = self._selectWindow.scaleChooser.document().toPlainText()
        if not (newScaleFile == ''):
            # Status message.
            currentStatus = self._translate('NetCDF', 'Drawing plot.')
            self._statusBar.showMessage(currentStatus)
            self._scaleFileName = newScaleFile
            #self._plot()
            self._computeScale()
            #self._actualPlot()
            # The map being displayed.
            tf = self._axis.tricontourf(self._tri_refi, self._field,
                                        levels = self._scaleLevels)
            self._colourBar.update_normal(tf)
            self.draw()

    def __init__ (self, parent, **kwargs):
        """
        Class initialiser.

        :param parent: Pointer to potential parent object.
        :param kwargs: command line arguments.
        """

        super(Main, self).__init__(parent, 'NetCDF')

        # Date chooser.
        self._dateChooser = Qt.QComboBox(self._parent)
        self._dateChooser.activated.connect(self._changeDate)

        # Field chooser.
        self._fieldChooser = Qt.QComboBox(self._parent)
        self._fieldChooser.activated.connect(self._changeField)

        # Scale file chooser button title.
        scaleTitle = self._translate('NetCDF', 'Change scale file')
        # Scale file chooser.
        self._scaleChooser = QPushButton(scaleTitle)
        self._scaleChooser.clicked.connect(self._changeScale)

    def __exit__ (self, exc_type, exc_val, exc_tb):
        """
        Resources deallocation.

        :param exc_type: Type of launched exception, if any.
        :param exc_val: Exception value.
        :param exc_tb: Exception executing trace.
        """

        self._ncFile.close()
        super(Main, self).__exit__()

    def _cancel (self):
        """
        Called when user cancel the operation.
        """

        pass

    def _proceed (self):
        """
        Action when user has defined what to plot.
        """

        # Status message.
        currentStatus = self._translate('NetCDF', 'Loading NetCDF data.')
        self._statusBar.showMessage(currentStatus)

        self._setPlottingArea()

        # Longitudes of grid nodes.
        x = self._ncFile.variables['longitude'][:]
        # Latitudes of grid nodes.
        y = self._ncFile.variables['latitude'][:]
        # Mesh elements.
        elements = self._ncFile.variables['tri'][:, :] - 1
        # Mesh to display.
        self._mesh = tri.Triangulation(x, y, elements)

        # Name of the field to be read.
        self._fieldName = self._selectWindow.fieldsSelector.currentText()
        # Iteration to plot.
        self._iteration = self._selectWindow.datesSelector.currentIndex()
        # Values in the field to be displayed.
        self._values = self._ncFile.variables[self._fieldName]
        self._dateChooser.setCurrentIndex(self._iteration)
        self._appToolBar.addWidget(self._dateChooser)
        self._fieldChooser.setCurrentText(self._fieldName)
        self._appToolBar.addWidget(self._fieldChooser)
        self._appToolBar.addWidget(self._scaleChooser)

        self._scaleFileName = \
            self._selectWindow.scaleChooser.document().toPlainText()
        # Whether or not the file is valid.
        invalidFile = (self._scaleFileName != '') \
            and ((not path.exists(self._scaleFileName)) \
                or (not path.isfile(self._scaleFileName)))
        if invalidFile:
            # Warning box title.
            warningBoxTitle = self._translate('NetCDF', 'Invalid file')
            # Warning box content.
            warningBoxContent = self._translate('NetCDF',
                                                'File named “{}” is invalid.')
            self._displayWarning(warningBoxTitle,
                                 warningBoxContent.format(self._scaleFileName))
            self._scaleFileName = ''

        self._plot()

    def open (self, fileName):
        """
        Open an open file menu to select the mesh file.

        :param fileName: Name of the file to be opened.
        """

        # Status message.
        currentStatus = self._translate('NetCDF', 'Loading NetCDF file.')
        self._statusBar.showMessage(currentStatus)

        # NetCDF file to be displayed
        self._ncFile = Dataset(fileName, mode = 'r')
        # Time for each computation iteration, in days since reference.
        deltas = self._ncFile.variables['time'][:].flatten()
        # Time format in computations.
        dFormat = '%Y-%m-%d %H:%M:%S'
        # Time reference in computations.
        tRef = datetime.strptime(self._ncFile.variables['time'].units[11:],
                                 dFormat)
        # Computation times.
        self._times = [tRef + timedelta(days = val) for val in deltas]

        # Selection window.
        self._selectWindow = uic.loadUi(path.dirname(path.realpath(__file__))
                                        + '/selection.ui')

        # List of keys that cannot be chosen to be displayed.
        exclude = ['longitude', 'latitude', 'time', 'tri']
        # Iteration identifiant.
        itId = 0
        # Iterate on every possible data fields.
        for varName in self._ncFile.variables.keys():
            if not (varName in exclude):
                self._selectWindow.fieldsSelector.addItem(varName, itId)
                self._fieldChooser.addItem(varName, itId)
                itId += 1

        itId = 0
        # Iterate on every possible dates.
        for date in self._times:
            # Current date in ISO standard format.
            currentDate = date.isoformat(' ')
            self._selectWindow.datesSelector.addItem(currentDate, itId)
            self._dateChooser.addItem(currentDate, itId)
            itId += 1

        self._selectWindow.show()

        self._selectWindow.buttonBox.accepted.connect(self._proceed)
        self._selectWindow.buttonBox.rejected.connect(self._cancel)
        self._selectWindow.fileSelect.clicked.connect(self._selectFile)

# What functionality this plugin provides.
provide = PluginsUtils.ProvideEnum.FileFormat
# NetCDF mesh file MIME type.
ncMIME = 'application/x-hdf'
# NetCDF mesh file extensions.
ncExtensions = ['*.nc', '*.NC']
# NetCDF mesh file identifiant.
ncId = QtCore.QCoreApplication.translate('MainWindow', 'NetCDF file')
# NetCDF mesh file describing tuple.
formatDescriptor = (ncMIME, ncExtensions, ncId)
