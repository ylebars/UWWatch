#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Compare computed solution with an observed time serie.

:module: PlotComp
:author: Le Bars, Yoann
:version: 1.1
:date: 2018/08/20

Copyright © 2018 Le Bars, Yoann

This program is distributed under CeCILL-B license, it can be copied and
modified freely as long as initial author is cited. Complete text
of CeCILL license can be found on-line:

<http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>
"""

from PyQt5 import QtCore
from PyQt5.QtWidgets import QPushButton, QMessageBox
from netCDF4 import Dataset
from datetime import datetime, timedelta
import numpy as np
from scipy import interpolate
from os import path
import PluginsUtils

# What functionality this plugin provides.
provide = PluginsUtils.ProvideEnum.FileFormat
# Comparison description file MIME type.
cmpMIME = 'text/plain'
# Comparison description file extensions.
cmpExtensions = ['*.cmp', '*.CMP']
# Comparison description file identifier.
cmpId = QtCore.QCoreApplication.translate('PlotComp', 'Comparison files')
# Gmsh mesh file describing tuple.
formatDescriptor = (cmpMIME, cmpExtensions, cmpId)

#def fillGaps (points, values, x0, y0, vMin, vMax):
    #"""
    #Fill gaps in a 2D scalar field.

    #:param points: Array of grid nodes.
    #:param values: Array to be filled.
    #:param x0: Abscissa where to interpolate.
    #:param y0: Ordinate where to interpolate.
    #:param vMin: Minimum valid value.
    #:param vMax: Maximum valid value.
    #:return: Value interpolated at location (x0, y0).
    #"""

    #values = np.asarray(values).flatten()
    # List of valid values.
    #valid = np.isfinite(values)
    # Corrected values.
    #cValues = [min(max(value, vMin), vMax) for value in values[valid]]
    #return interpolate.griddata(points[valid], cValues, (x0, y0),
                                #method = 'cubic')

def rmse (computed, observed):
    """
    Compute root mean square error between two set of values.

    :param computed: Set of computed values.
    :param observed: Set of observed values.
    :return: Root mean square error of computed values compare to observed
             values.
    """

    return np.sqrt(((computed - observed)**2).mean())

class Main (PluginsUtils.Main):
    """
    Class for comparison plots.
    """

    def _clickButton (self):
        """
        Display RMSE.
        """

        # Message box title.
        mBoxTitle = self._translate('PlotComp', 'RMSE')
        # Message box content.
        mBoxContent = ''
        for i in range(len(self._labels)):
            # String for current label.
            itemString = self._translate('PlotComp', '<p>{}: {}</p>')
            mBoxContent += itemString.format(self._labels[i], self._lRMSE[i])
        QMessageBox.about(self, mBoxTitle, mBoxContent)

    def __init__ (self, parent, **kwargs):
        """
        Class initialiser.

        :param parent: Pointer to potential parent object.
        :param kwargs: command line arguments.
        """

        super(Main, self).__init__(parent, 'Series comparison')

        title = self._translate('PlotComp', 'RMSE')
        # Choose to display mesh statistics.
        self._statButton = QPushButton(title)
        self._statButton.clicked.connect(self._clickButton)

    def open (self, fileName):
        """
        Open comparison description file and plot comparison.

        :param fileName: Name of the file to be opened.
        """

        # Status message.
        currentStatus = self._translate('PlotComp',
                                        'Loading comparison description file.')
        self._statusBar.showMessage(currentStatus)

        self._appToolBar.addWidget(self._statButton)

        self._setPlottingArea()
        self._axis = self._figure.add_subplot(1,1,1)

        if (not path.exists(fileName)) or (not path.isfile(fileName)):
            raise PluginsUtils.InexistingFile(fileName)

        # Dictionary of arrays to be exported.
        dictExport = {}

        # File describing comparison to be done.
        with open(fileName, 'r') as comparisonFile:
            # Format type and version.
            fileFormat, formatVersion = comparisonFile.readline().split()
            if not (fileFormat == 'PlotComp'):
                raise PluginsUtils.FileError(fileName)
            if not (formatVersion == '1.0'):
                raise PluginsUtils.UnsupportedVersion(cmpId, formatVersion,
                                                      fileName)

            # Compared field description.
            fieldDesc = comparisonFile.readline()

            # Current line to be processed.
            line = comparisonFile.readline().split()
            # Observation file name.
            oFileName = line[0]
            if (not path.exists(oFileName)) or (not path.isfile(oFileName)):
                raise PluginsUtils.InexistingFile(oFileName)
            # Observation field name.
            oField = line[1]
            # Observation label in figure.
            oLabel = ' '.join(line[2:])
            # File containing observed data.
            with Dataset(oFileName, mode = 'r') as file:
                if not oField in file.variables.keys():
                    raise PluginsUtils.InexistingField(oField, oFileName)
                # Longitude where to interpolate.
                x0 = file.variables['LONGITUDE'][0].flatten()
                # Latitude where to interpolate.
                y0 = file.variables['LATITUDE'][0].flatten()
                # Observed values.
                oValues = file.variables[oField][:, 0].flatten()
                # Time for each data, in days since reference.
                deltas = file.variables['TIME'][:].flatten()
                # Time format in observations.
                dFormat = '%Y-%m-%dT%H:%M:%SZ'
                # Time reference in observations.
                oTRef = datetime.strptime(file.variables['TIME'].units[11:],
                                          dFormat)
                # Progress bar title.
                pBarTitle = self._translate('PlotComp',
                                            'Getting observation dates.')
                # Progress bar indicating advancement computations.
                pBar = PluginsUtils.ProgressBar(pBarTitle)
                # Observation times.
                oTimes = [oTRef + timedelta(days = val) for val in pBar(deltas)]

                # Figure title
                figTitle = self._translate('PlotComp', '{}° N, {}° E')
                self._axis.set_title(figTitle.format(y0[0], x0[0]))
                # Date description.
                dateDesc = self._translate('PlotComp', 'Date')
                self._axis.set_xlabel(dateDesc)
                self._axis.plot(oTimes, oValues, label = oLabel)

                dictExport[oField] = oValues

            # Whether or not it is the first file to be dealt with.
            first = True
            # Solution number.
            sNumber = 0
            # List of labels.
            self._labels = []
            # List of RMSE.
            self._lRMSE = []
            for line in comparisonFile:
                sNumber = sNumber + 1
                # Line as a list.
                listLine = line.split()
                # Name of current solution file.
                fileName = listLine[0]
                if (not path.exists(fileName)) or (not path.isfile(fileName)):
                    raise PluginsUtils.InexistingFile(fileName)
                # Field to test.
                field = listLine[1]
                # Label in figure.
                label = ' '.join(listLine[2:])
                self._labels.append(label)
                #toto = inter.inter2D(fileName, field, float(x0[0]),
                                     #float(y0[0]))
                cTRef, cTimes, cValues = PluginsUtils.drawSerie(fileName,
                                                                field,
                                                                sNumber, x0,
                                                                y0, first,
                                                                self._axis,
                                                                fieldDesc,
                                                                label,
                                                                dictExport)

                # Time reference for RMSE computation.
                tRef = min(oTRef, cTRef)
                # Relative observation times.
                rOTime = [(value - tRef).total_seconds() for value in oTimes]
                # Relative computation times.
                rCTime = [(value - tRef).total_seconds() for value in cTimes]

                # Flattened computed values.
                fValues = np.asarray(cValues).flatten()
                # List of valid values.
                valid = np.isfinite(fValues)
                # Corrected valid values.
                cFValues = np.asarray(fValues[valid]).flatten()
                # Corrected relative computing times.
                cRCTime = []
                pBarTitle = self._translate('PlotComp',
                                            'Selecting time steps for RMSE.')
                pBar.reset(pBarTitle)
                for i in pBar(range(len(valid))):
                    if valid[i]:
                        cRCTime.append(rCTime[i])

                # Minimum time step.
                minTS = min(cRCTime)
                # Maximum time step.
                maxTS = max(cRCTime)
                # Interpolating function.
                iValues = interpolate.interp1d(cRCTime, cFValues,
                                               kind = 'cubic')
                # Select time step.
                selectedTimeSteps = []
                # Selected observed values.
                selectedOValues = []
                pBarTitle = self._translate('PlotComp',
                                            'Getting values for RMSE.')
                pBar.reset(pBarTitle)
                for i in pBar(range(len(rOTime))):
                    if (rOTime[i] >= minTS) and (rOTime[i] <= maxTS) \
                        and (np.isfinite(oValues[i])):
                            selectedTimeSteps.append(rOTime[i])
                            selectedOValues.append(oValues[i])
                # Interpolated values.
                interpolated = iValues(selectedTimeSteps)
                self._lRMSE.append(rmse(interpolated, selectedOValues))

        # Legends in the plot
        legend = self._axis.legend()

        self.draw()

        self._setExportMat(dictExport)

        self._statusBar.showMessage(self._readyMessage)
