#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Initialisation of PlotSeries module.

:module: PlotSeries
:author: Le Bars, Yoann
:version: 1.0
:date: 2019/02/07

Copyright © 2019 Le Bars, Yoann

This program is distributed under CeCILL-B license, it can be copied and
modified freely as long as initial author is cited. Complete text
of CeCILL license can be found on-line:

<http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>
"""

from plotSeries import Main, formatDescriptor, provide
