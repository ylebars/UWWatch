#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Compare computed solution with an observed time serie.

:module: PlotSeries
:author: Le Bars, Yoann
:version: 1.0
:date: 2019/02/08

Copyright © 2019 Le Bars, Yoann

This program is distributed under CeCILL-B license, it can be copied and
modified freely as long as initial author is cited. Complete text
of CeCILL license can be found on-line:

<http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>
"""

from PyQt5 import QtCore
from os import path
import PluginsUtils

# What functionality this plugin provides.
provide = PluginsUtils.ProvideEnum.FileFormat
# Comparison description file MIME type.
pltMIME = 'text/plain'
# Comparison description file extensions.
pltExtensions = ['*.plt', '*.PLT']
# Comparison description file identifier.
pltId = QtCore.QCoreApplication.translate('PlotSeries', 'Plotting time series at a given location')
# Gmsh mesh file describing tuple.
formatDescriptor = (pltMIME, pltExtensions, pltId)

class Main (PluginsUtils.Main):
    """
    Class for comparison plots.
    """

    def __init__ (self, parent, **kwargs):
        """
        Class initialiser.

        :param parent: Pointer to potential parent object.
        :param kwargs: command line arguments.
        """

        super(Main, self).__init__(parent, 'Plotting series')

    def open (self, fileName):
        """
        Open series plotting description file and plot them.

        :param fileName: Name of the file to be opened.
        """

        # Status message.
        currentStatus = self._translate('PlotSeries',
                                        'Loading plotting description file.')
        self._statusBar.showMessage(currentStatus)

        self._setPlottingArea()
        self._axis = self._figure.add_subplot(1,1,1)

        if (not path.exists(fileName)) or (not path.isfile(fileName)):
            raise PluginsUtils.InexistingFile(fileName)

        # Dictionary of arrays to be exported.
        dictExport = {}

        # File describing comparison to be done.
        with open(fileName, 'r') as plottingFile:
            # Format type and version.
            fileFormat, formatVersion = plottingFile.readline().split()
            if not (fileFormat == 'PlotSeries'):
                raise PluginsUtils.FileError(fileName)
            if not (formatVersion == '1.0'):
                raise PluginsUtils.UnsupportedVersion(cmpId, formatVersion,
                                                      fileName)

            # Compared field description.
            fieldDesc = plottingFile.readline()
            # Plotting localisation as strings.
            lonS, latS = plottingFile.readline().split()
            # Plotting longitude.
            lon = float(lonS)
            # Plotting latitude.
            lat = float(latS)

            # Figure title
            figTitle = self._translate('PlotSeries', '{}° N, {}° E')
            self._axis.set_title(figTitle.format(lat, lon))

            # Whether or not it is the first file to be dealt with.
            first = True
            # File number.
            fNumber = 0
            # List of labels.
            self._labels = []
            for line in plottingFile:
                fNumber = fNumber + 1
                # Line as a list.
                listLine = line.split()
                # Name of current solution file.
                fileName = listLine[0]
                if (not path.exists(fileName)) or (not path.isfile(fileName)):
                    raise PluginsUtils.InexistingFile(fileName)
                # Field to test.
                field = listLine[1]
                # Label in figure.
                label = ' '.join(listLine[2:])
                self._labels.append(label)
                cTRef, cTimes, cValues = PluginsUtils.drawSerie(fileName,
                                                                field,
                                                                fNumber, lon,
                                                                lat, first,
                                                                self._axis,
                                                                fieldDesc,
                                                                label,
                                                                dictExport)

        # Legends in the plot
        legend = self._axis.legend()

        self.draw()

        self._setExportMat(dictExport)

        self._statusBar.showMessage(self._readyMessage)
