#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Several useful elements to ease plugins development.

:module: Gmsh
:author: Le Bars, Yoann
:version: 1.1
:date: 2018/06/18

Copyright © 2018 Le Bars, Yoann

This program is distributed under CeCILL-B license, it can be copied and
modified freely as long as initial author is cited. Complete text
of CeCILL license can be found on-line:

<http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html>
"""

from PyQt5.QtWidgets import (
    QWidget, QSizePolicy, QProgressDialog, QFileDialog, QAction
)
from PyQt5 import QtCore
import matplotlib.pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from enum import Enum
import numpy as np
from scipy import interpolate
from math import radians, cos, sin, asin, sqrt
from netCDF4 import Dataset
from datetime import datetime, timedelta
import hdf5storage

class PluginError (Exception):
    """
    Base exception class for plugins errors.
    """

    def __init__ (self, message):
        """
        Class initialiser.
        """

        self.message = message

class FileError (PluginError):
    """
    Exception when file format is invalid.
    """

    def __init__ (self, fileName):
        """
        Class initialiser.

        :param fileName: Name of the file which caused the exception.
        """

        self.fileName = fileName

class UnsupportedVersion (PluginError):
    """
    Exception raised when a file format version is not supported.
    """

    def __init__ (self, fileFormat, version, fileName):
        """
        Class initialiser.

        :param fileFormat: Format file which version is not supported.
        :param version: version of the format.
        :param fileName: Name of the file which caused the exception.
        """

        self.fileFormat = fileFormat
        self.version = version
        self.fileName = fileName

class InexistingFile (PluginError):
    """
    Exception when a file does not exist.
    """

    def __init__ (self, fileName):
        """
        Class initialiser.

        :param fileName: Name of the file which caused the exception.
        """

        self.fileName = fileName

class InexistingField (PluginError):
    """
    Exception when a field does not exist in a NetCDF file.
    """

    def __init__(self, fieldName, fileName):
        """
        Class initialiser.

        :param fieldName: Name of the field which caused the exception.
        :param fileName: Name of the NetCDF file which caused the exception.
        """

        self.fieldName = fieldName
        self.fileName = fileName

class ProgressBar (QProgressDialog):
    """
    Class providing an easy way to use QProgressDialog.
    """

    def __init__ (self, title, minValue = 0, maxValue = None):
        """
        Class initialiser.
        
        :param title: Window title.
        :param minValue: Minimum value in iteration.
        :param maxValue: Maximum value in iteration.
        """

        super().__init__(title, None, minValue, 0)
        self.setMinimumDuration(0)
        self.setModal(True)
        if maxValue is not None:
            self.setMaximum(maxValue)
        # Iterable through which iterate.
        self._iterable = None
        self.setValue(self.minimum())
        # Window title.
        windowTitle = QtCore.QCoreApplication.translate('ProgressBar',
                                                        'Progression')
        self.setWindowTitle(windowTitle)
        # Whether or not iteration has started.
        self._started = False

    def __call__ (self, iterable, maxValue = None):
        """
        Use a ProgressBar to iterate through an iterable.

        
        :param iterable: The iterable through which iterate.
        :param maxValue: Maximum value in iteration.
        :return: A pointer to current instance.
        """

        if maxValue is None:
            self.setMaximum(len(iterable))
        else:
            self.setMaximum(maxValue)

        self._iterable = iter(iterable)
        self.show()
        return self

    def __iter__ (self):
        """
        Defines the class as iterable.

        :return: A pointer to current instance.
        """

        return self

    def __next__ (self):
        """
        What to be done every next iteration.
        """

        try:
            if self.wasCanceled():
                raise StopIteration ()
            # New value in iteration.
            iter = next(self._iterable)
            if self._started:
                self.setValue(self.value() + 1)
            else:
                self._started = True
                self.setValue(self.minimum())
            return iter
        except StopIteration:
            self._started = False
            self.setValue(self.maximum())
            self.cancel()
            raise

    def __exit__ (self, exc_type, exc_value, traceback):
        """
        What to do when exiting ProgressBar.

        :param exc_type: Type of execution.
        :param exc_value: Execution code.
        :param traceback: Execution trace.
        """

        pass

    def __enter__ (self):
        """
        What to do when entering ProgressBar.

        :return: A pointer to current instance.
        """

        return self

    def reset (self, title, valMin = 0, valMax = None):
        """
        Reset progress bar.

        :param title: New window title.
        :param valMin: Minimum value during progress.
        :param valMax: Maximum value during progress.
        """

        super().reset()
        self.setLabelText(title)
        self.setMinimum(valMin)
        if valMax is not None:
            self.setMaximum(valMax)
        self.setValue(valMin)
        self._started = False

class Main (QWidget):
    """
    Base class for main class of any plug-in
    """

    def __init__ (self, parent, name):
        """
        Class initialiser.

        :param parent: Pointer to class parent.
        :param name: Plug-in name.
        """

        # Parent component.
        self._parent = parent
        # Application status bar.
        self._statusBar = self._parent.statusBar
        # Translation shortcut.
        self._translate = self._parent.translate

        # Application status message.
        currentState = self._translate('PluginsUtils',
                                       name + ' module initialisation.')
        self._statusBar.showMessage(currentState)

        super(Main, self).__init__(self._parent)
        QWidget.__init__(self)

        # Message indicating the application is ready.
        self._readyMessage = self._parent.readyMessage
        # Application drawing area.
        self._parentDrawingArea = parent.drawingArea
        # Tool bar layout.
        self._toolBarLayout = parent.figureLayout
        # Application tool bar.
        self._appToolBar = parent.toolBar
        # Method displaying a warning box.
        self._displayWarning = parent.displayWarning

    def __enter__ (self):
        """
        Return current instance.

        :return: A pointer to current instance.
        """

        return self

    def __exit__ (self):
        """
        Plug-in deallocation.
        """

        plt.close(self._figure)

    def _setPlottingArea (self):
        """
        Setting plugin plotting area.
        """

        # Matplotlib drawing area.
        self._figure = plt.figure()
        # Link to interface drawing area.
        self._drawingArea = FigureCanvas(self._figure)
        self._drawingArea.setSizePolicy(QSizePolicy.Expanding,
                                        QSizePolicy.Expanding)
        self._drawingArea.updateGeometry()
        self._parentDrawingArea.addWidget(self._drawingArea)
        # Figure tool bar.
        self._toolBar = NavigationToolbar(self._drawingArea, self)
        self._toolBarLayout.addWidget(self._toolBar)

    def draw (self):
        """
        Draw picture.
        """

        self._figure.canvas.draw()

    def _setAxis (self):
        """
        Set figure axis.
        """

        # Figure axis.
        self._axis = self._figure.add_subplot(1,1,1)
        self._axis.set_aspect('equal')

    def _exportMat (self):
        """
        Export time series to Matlab file.
        """

        # Title of the dialog box.
        title = self._translate('PluginsUtils', 'Export in Matlab format')
        # List of possible file format.
        formats = self._translate('PluginsUtils',
                                  'Matlab file (*.mat);;All files (*)')
        # Path to the file to save.
        filePath = QFileDialog.getSaveFileName(self._parent, title, '',
                                               formats)[0]
        hdf5storage.savemat(filePath, self._dictExport)

    def _setExportMat (self, dictExport):
        """
        Add a menu entry to export in Matlab file format.

        :param dictExport: Dictionnary describing arrays to be exported.
        """

        self._dictExport = dictExport
        # Action name.
        aName = self._translate('PluginsUtils', 'E&xport in Matlab file format')
        actionExport = QAction(aName, self._parent)
        actionExport.setShortcut('Ctrl+X')
        actionExport.triggered.connect(self._exportMat)
        self._parent._ui.menuFile.addSeparator()
        self._parent._ui.menuFile.addAction(actionExport)

def interpolateGaps (values, limit = None):
    """
    Fill gaps using linear interpolation, optionally only fill gaps up to a
    size of ‘limit’.

    :param values: Array of value to be interpolated.
    :param x: Array of abscissa.
    :param y: Array of ordinates.
    :param limit: Optional limit of gaps to be filled.

    :return: An array containing values interpolated.
    """

    values = np.asarray(values).flatten()
    # Set of indices for tab.
    i = np.arange(values.size)
    # List of valid values.
    valid = np.isfinite(values)
    # Interpolation function.
    function = interpolate.interp1d(i[valid], values[valid], kind = 'cubic')
    # Filled set of values.
    filled = function(i)

    if limit is not None:
        # List of invalid values.
        invalid = ~valid
        for n in range(1, limit + 1):
            invalid[:-n] &= invalid[n:]
        filled[invalid] = np.nan

    return filled

def geoDist(lon1, lat1, lon2, lat2):
    """
    Compute distance between two point knowing their localisation on the
    surface of the Earth.

    :param lon1: Longitude of the first point.
    :param lat1: Latitude of the first point.
    :param lon2: Longitude of the second point.
    :param lat2: Latitude of the second point.
    """

    # From degree to radian.
    lon1 = radians(lon1)
    lon2 = radians(lon2)
    lat1 = radians(lat1)
    lat2 = radians(lat2)

    # Haversine formula
    # Difference between the two longitudes.
    dlon = lon2 - lon1 
    # Difference between the two latitudes.
    dlat = lat2 - lat1 
    # Projection.
    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    # Normalisation.
    c = 2. * asin(sqrt(a)) 
    # Radius of the Earth in meters.
    r = 6371008.

    return c * r

def fillGaps (points, values, x0, y0, vMin, vMax):
    """
    Fill gaps in a 2D scalar field.

    :param points: Array of grid nodes.
    :param values: Array to be filled.
    :param x0: Abscissa where to interpolate.
    :param y0: Ordinate where to interpolate.
    :param vMin: Minimum valid value.
    :param vMax: Maximum valid value.
    :return: Value interpolated at location (x0, y0).
    """

    values = np.asarray(values).flatten()
    # List of valid values.
    valid = np.isfinite(values)
    # Corrected values.
    cValues = [min(max(value, vMin), vMax) for value in values[valid]]
    return interpolate.griddata(points[valid], cValues, (x0, y0),
                                method = 'cubic')

def drawSerie (fileName, field, sNumber, x0, y0, first, axis, fieldDesc,
               label, dictExport):
    """
    Draw temporal series from a NetCDF result file from WaveWatch.

    :param fileName: Name of the file from which extract data to draw.
    :param field: Name of the field to be interpolated.
    :param sNumber: Number of the file to be plotted.
    :param x0: Abscissa where to interpolate.
    :param y0: Ordinate where to interpolate.
    :param first: Whether or not the file is the first one to be dealt with.
    :param axis: Figure on which results have to be plotted.
    :param fieldDesc: Plot legend.
    :param label: Current curve label.
    :param dictExport: Dictionary of arrays to be exported into Matlab files.
    :return: Time reference for the given solution.
    """

    # File containing computed solution.
    with Dataset(fileName, mode = 'r') as cFile:
        if field not in cFile.variables.keys():
            raise InexistingField(field, fileName)
        # Longitudes of grid nodes.
        x = cFile.variables['longitude'][:]
        # Latitudes of grid nodes.
        y = cFile.variables['latitude'][:]
        # Node list.
        points = np.array((x.flatten(), y.flatten())).T
        # Values in the field to be displayed.
        values =  cFile.variables[field]
        # Title for progress bar.
        pBarTitle = QtCore.QCoreApplication.translate('PluginsUtils',
                                                      'Getting computed '
                                                      'values in '
                                                      'solution {}.')
        # Progress bar indicating progress in computations.
        pBar = ProgressBar(pBarTitle.format(sNumber))
        # Computed values.
        cValues = [fillGaps(points, values[i], x0, y0, values.valid_min,
                            values.valid_max)
                   for i in pBar(range(cFile.dimensions['time'].size))]
        # Delta between computing date and simulation starting date.
        deltas = cFile.variables['time'][:].flatten()
        # Format for date.
        dFormat = '%Y-%m-%d %H:%M:%S'
        # Time reference in computations.
        cTRef = datetime.strptime(cFile.variables['time'].units[11:], dFormat)
        pBarTitle = QtCore.QCoreApplication.translate('PlotComp',
                                                      'Getting computation '
                                                      'dates in solution {}.')
        pBar.reset(pBarTitle.format(sNumber))
        # Computation times.
        cTimes = [cTRef + timedelta(days = val) for val in pBar(deltas)]

        if first:
            axis.set_ylabel('{} ({})'.format(fieldDesc, values.units))
            dictExport['dates'] = deltas
            first = False
        axis.plot(cTimes, cValues, label = label)
        dictExport[field + str(sNumber)] = cValues

    return cTRef, cTimes, cValues

# Enumeration of services types provided by plugins.
ProvideEnum = Enum('ProvideEnum', 'Utils FileFormat')

# What functionality this plugin provides.
provide = ProvideEnum.Utils
