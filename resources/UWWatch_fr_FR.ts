<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en_GB">
<context>
    <name>Dialog</name>
    <message>
        <location filename="../plugins/NetCDF/selection.ui" line="42"/>
        <source>Date:</source>
        <translation>Date :</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/selection.ui" line="55"/>
        <source>Field:</source>
        <translation>Champ :</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/selection.ui" line="68"/>
        <source>Scale:</source>
        <translation>Échelle :</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/selection.ui" line="116"/>
        <source>Choose</source>
        <translation>Sélectionner</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/selection.ui" line="14"/>
        <source>Select</source>
        <translation>Selection</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="main.ui" line="14"/>
        <source>UWWatch</source>
        <translation>UWWatch</translation>
    </message>
    <message>
        <location filename="main.ui" line="45"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="main.ui" line="57"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <location filename="main.ui" line="68"/>
        <source>toolBar</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main.ui" line="87"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="main.ui" line="90"/>
        <source>Exit UWWatch</source>
        <translation>Quitter UWWatch</translation>
    </message>
    <message>
        <location filename="main.ui" line="93"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main.ui" line="98"/>
        <source>About UWWatch</source>
        <translation>À propos de UWWatch</translation>
    </message>
    <message>
        <location filename="main.ui" line="103"/>
        <source>About Qt</source>
        <translation>À propos de Qt</translation>
    </message>
    <message>
        <location filename="main.ui" line="112"/>
        <source>&amp;Open file</source>
        <translation>&amp;Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="main.ui" line="115"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="main.ui" line="124"/>
        <source>&amp;Close window</source>
        <translation>&amp;Fermer la fenêtre</translation>
    </message>
    <message>
        <location filename="main.ui" line="127"/>
        <source>Close current window.</source>
        <translation>Ferme la fenêtre courrante.</translation>
    </message>
    <message>
        <location filename="main.ui" line="130"/>
        <source>Ctrl+W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="69"/>
        <source>About UWWatch desk</source>
        <translation>À propos de l’atelier UWWatch</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="96"/>
        <source>Unsupported format</source>
        <translation>Format non supporté</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="98"/>
        <source>&lt;p&gt;File format not supported.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Format de fichier non supporté.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="358"/>
        <source>Invalid file</source>
        <translation>Fichier invalide</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="147"/>
        <source>Unsupported version</source>
        <translation>Version non supportée</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="178"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="193"/>
        <source>Opening file.</source>
        <translation>Ouverture d’un fichier.</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="199"/>
        <source>Open file</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="202"/>
        <source>All files (*)</source>
        <translation>Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="320"/>
        <source>Initialisation.</source>
        <translation>Initialisation.</translation>
    </message>
    <message>
        <location filename="../plugins/Gmsh/gmsh.py" line="401"/>
        <source>Gmsh mesh</source>
        <translation>Maillage Gmsh</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="401"/>
        <source>NetCDF file</source>
        <translation>Fichier NetCDF</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="336"/>
        <source>Ready.</source>
        <translation>Prêt.</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="168"/>
        <source>Invalid field</source>
        <translation>Champ invalide</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="140"/>
        <source>&lt;p&gt;Invalid format in file named “{}”.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Le format du fichier nommé « {} » est invalide.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="149"/>
        <source>&lt;p&gt;File named “{}” is in format {} version {}, which is not supported.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Le fichier nommé « {} » est au format {} version {}, lequel format n’est pas supporté.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="162"/>
        <source>&lt;p&gt;File named “{}” is invalid.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Le fichier nommé « {} » est invalide.&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../main/uwwatch.py" line="170"/>
        <source>&lt;p&gt;Field named “{}” does not exists in file “{}”.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Le champ nommé « {} » n’existe pas dans le fichier « {} ».&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>Mesh</name>
    <message>
        <location filename="../plugins/Gmsh/gmsh.py" line="290"/>
        <source>Loading mesh file.</source>
        <translation>Charge un maillage.</translation>
    </message>
    <message>
        <location filename="../plugins/Gmsh/gmsh.py" line="39"/>
        <source>Drawing mesh.</source>
        <translation>Trace le maillage.</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="106"/>
        <source>Longitude (degrees)</source>
        <translation>Longitude (degrés)</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="108"/>
        <source>Latitude (degrees)</source>
        <translation>Latitude (degrés)</translation>
    </message>
    <message>
        <location filename="../plugins/Gmsh/gmsh.py" line="380"/>
        <source>Interpolating bathymetry.</source>
        <translation>Interpolation de la bathymétrie.</translation>
    </message>
    <message>
        <location filename="../plugins/Gmsh/gmsh.py" line="49"/>
        <source>Bathymetry (in m)</source>
        <translation>Bathymétrie (en m)</translation>
    </message>
    <message>
        <location filename="../plugins/Gmsh/gmsh.py" line="57"/>
        <source>Mesh</source>
        <translation>Maillage</translation>
    </message>
    <message>
        <location filename="../plugins/Gmsh/gmsh.py" line="373"/>
        <source>Creating figure.</source>
        <translation>Création de la figure.</translation>
    </message>
    <message>
        <location filename="../plugins/Gmsh/gmsh.py" line="327"/>
        <source>Reading file.</source>
        <translation>Ouverture du fichier.</translation>
    </message>
    <message>
        <location filename="../plugins/Gmsh/gmsh.py" line="277"/>
        <source>Mesh statistics</source>
        <translation>Statistiques du maillage</translation>
    </message>
    <message>
        <location filename="../plugins/Gmsh/gmsh.py" line="259"/>
        <source>Display bathymetry</source>
        <translation>Afficher la bathymétrie</translation>
    </message>
    <message>
        <location filename="../plugins/Gmsh/gmsh.py" line="270"/>
        <source>Display node labels</source>
        <translation>Afficher index des nœuds</translation>
    </message>
    <message>
        <location filename="../plugins/Gmsh/gmsh.py" line="233"/>
        <source>&lt;p&gt;Number of nodes: {}&lt;/p&gt;&lt;p&gt;Number of triangular elements: {}&lt;/p&gt;&lt;p&gt;Minimum element size: {} m&lt;/p&gt;&lt;p&gt;Maximum element size: {} m&lt;/p&gt;&lt;p&gt;Mean element size: {} m&lt;/p&gt;&lt;p&gt;Median element size: {} m&lt;/p&gt;&lt;p&gt;Zoom box min: {}° N, {}° E&lt;/p&gt;&lt;p&gt;Zoom box max: {}° N, {}° E&lt;/p&gt;</source>
        <translation>&lt;p&gt;Nombre de nœuds : {}&lt;/p&gt;&lt;p&gt;Nombre d’éléments triangulaires : {}&lt;/p&gt;&lt;p&gt;Taille d’élément minimale : {} m&lt;/p&gt;&lt;p&gt;Taille d’élément maximale : {} m&lt;/p&gt;&lt;p&gt;Taille d’élément moyenne : {} m&lt;/p&gt;&lt;p&gt;Taille d’élément médianne : {} m&lt;/p&gt;&lt;p&gt;Localisation minimum du zoom: {}° N, {}° E&lt;/p&gt;&lt;p&gt;Localisation maximum du zoom : {}° N, {}° E&lt;/p&gt;</translation>
    </message>
</context>
<context>
    <name>NetCDF</name>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="349"/>
        <source>Loading NetCDF file.</source>
        <translation>Charge un fichier NetCDF.</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="151"/>
        <source>Values interpolation.</source>
        <translation>Interpolation.</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="296"/>
        <source>Loading NetCDF data.</source>
        <translation>Traite les données NetCDF.</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="330"/>
        <source>Invalid file</source>
        <translation>Fichier invalide</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="220"/>
        <source>Open scale file</source>
        <translation>Ouverture d’un fichier d’échelle</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="235"/>
        <source>Drawing plot.</source>
        <translation>Traçage de la figure.</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="147"/>
        <source>Computing interpolation.</source>
        <translation>Calcul de l’interpolation.</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="266"/>
        <source>Change scale file</source>
        <translation>Changer l’échelle de couleur</translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="49"/>
        <source>Correcting valid values.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="61"/>
        <source>Interpolating invalid values.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../plugins/NetCDF/netcdf.py" line="332"/>
        <source>File named “{}” is invalid.</source>
        <translation>Le fichier nommé « {} » est invalide.</translation>
    </message>
</context>
<context>
    <name>PlotComp</name>
    <message>
        <location filename="../plugins/PlotComp/plotComp.py" line="121"/>
        <source>Loading comparison description file.</source>
        <translation>Lecture du fichier de description de comparaisons.</translation>
    </message>
    <message>
        <location filename="../plugins/PlotComp/plotComp.py" line="177"/>
        <source>Getting observation dates.</source>
        <translation>Lecture des dates d’observations.</translation>
    </message>
    <message>
        <location filename="../plugins/PluginsUtils/pluginsUtils.py" line="476"/>
        <source>Getting computation dates in solution {}.</source>
        <translation>Lecture des dates de simulation dans la solution {}.</translation>
    </message>
    <message>
        <location filename="../plugins/PlotComp/plotComp.py" line="40"/>
        <source>Comparison files</source>
        <translation>Comparaison de séries</translation>
    </message>
    <message>
        <location filename="../plugins/PlotComp/plotComp.py" line="188"/>
        <source>Date</source>
        <translation>Date</translation>
    </message>
    <message>
        <location filename="../plugins/PlotComp/plotComp.py" line="108"/>
        <source>RMSE</source>
        <translation>RMSE</translation>
    </message>
    <message>
        <location filename="../plugins/PlotComp/plotComp.py" line="94"/>
        <source>&lt;p&gt;{}: {}&lt;/p&gt;</source>
        <translation>&lt;p&gt;{} : {}&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../plugins/PlotComp/plotComp.py" line="241"/>
        <source>Selecting time steps for RMSE.</source>
        <translation>Selection des pas de temps pour le calcul de la RMSE.</translation>
    </message>
    <message>
        <location filename="../plugins/PlotComp/plotComp.py" line="259"/>
        <source>Getting values for RMSE.</source>
        <translation>Récupération des valeurs pour le calcul de la RMSE.</translation>
    </message>
    <message>
        <location filename="../plugins/PlotComp/plotComp.py" line="185"/>
        <source>{}° N, {}° E</source>
        <translation>{} ° N, {} ° E</translation>
    </message>
</context>
<context>
    <name>PlotSeries</name>
    <message>
        <location filename="../plugins/PlotSeries/plotSeries.py" line="32"/>
        <source>Plotting time series at a given location</source>
        <translation>Tracer séries temporelles.</translation>
    </message>
    <message>
        <location filename="../plugins/PlotSeries/plotSeries.py" line="59"/>
        <source>Loading plotting description file.</source>
        <translation>Lecture du fichier de description de la série.</translation>
    </message>
    <message>
        <location filename="../plugins/PlotSeries/plotSeries.py" line="92"/>
        <source>{}° N, {}° E</source>
        <translation>{} ° N, {} ° E</translation>
    </message>
</context>
<context>
    <name>PluginsUtils</name>
    <message>
        <location filename="../plugins/PluginsUtils/pluginsUtils.py" line="460"/>
        <source>Getting computed values in solution {}.</source>
        <translation>Lecture des valeurs calculées dans la solution {}.</translation>
    </message>
    <message>
        <location filename="../plugins/PluginsUtils/pluginsUtils.py" line="320"/>
        <source>Export in Matlab format</source>
        <translation>Exporter au format Matlab</translation>
    </message>
    <message>
        <location filename="../plugins/PluginsUtils/pluginsUtils.py" line="322"/>
        <source>Matlab file (*.mat);;All files (*)</source>
        <translation>Fichiers Matlab (*.mat);;Tous les fichiers (*)</translation>
    </message>
    <message>
        <location filename="../plugins/PluginsUtils/pluginsUtils.py" line="338"/>
        <source>E&amp;xport in Matlab file format</source>
        <translation>E&amp;xporter au format Matlab</translation>
    </message>
</context>
<context>
    <name>ProgressBar</name>
    <message>
        <location filename="../plugins/PluginsUtils/pluginsUtils.py" line="133"/>
        <source>Progression</source>
        <translation>Progression</translation>
    </message>
</context>
</TS>
