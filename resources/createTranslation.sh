#!/bin/sh

# Script to create translation file. The last parameter is the translation
# file. You need to change “fr_FR” by the localisation code for the translation
# you want to make.

pylupdate5 -noobsolete ../main/uwwatch.py main.ui ../plugins/Gmsh/gmsh.py \
	../plugins/NetCDF/netcdf.py ../plugins/NetCDF/selection.ui \
	../plugins/PluginsUtils/pluginsUtils.py ../plugins/PlotComp/plotComp.py \
	../plugins/PlotSeries/plotSeries.py -ts UWWatch_fr_FR.ts

# Then, open the *.ts file with Qt Linguist. When the translation is done, in
# Qt Linguist, execute File > Release.
